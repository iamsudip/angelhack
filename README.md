AngelHack
=========

Project requirement
-------------------

Fetch all startup info and store


Requirements
------------

Deprecated this dependency(PyYAML) for angelhack: 28th Jan'13

PyYAML::

    $ pip install PyYAML

requests::

    $ pip install requests

    
How to run
----------

To run simply do::

	$ python angelhack.py

*If you are using the daemon follow below*

To start the daemon ::

    $ python angelhack.py start

this will start inserting the articles to the database(Assuming the mongodb database is running).
It will log the data inserted to the database in a file named angelhack.log.

To stop the daemon ::

    $ python angelhack.py stop


Press [ctrl]+c to stop the process.


Example run
-----------

    $ python angelhack.py

*For daemonised code*

    $ python angelhack.py start
    PID: 3418 Daemon started successfully

    $ python angelhack.py stop
    Daemon killed succesfully


